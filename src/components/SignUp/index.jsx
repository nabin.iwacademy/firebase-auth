import React from "react";
import { Link, withRouter } from "react-router-dom";
import { compose } from "recompose";
import { withFirebase } from "../Firebase";
import * as ROUTES from "../../constants/Routes";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
} from "semantic-ui-react";

const SignUpPage = () => {
  return (
    <div className="ui two column centered grid">
      <SignUpForm />
    </div>
  );
};
const INITIAL_STATE = {
  username: "",
  email: "",
  passwordOne: "",
  passwordTwo: "",
  error: null,
};

class SignUpFormBase extends React.Component {
  state = { ...INITIAL_STATE };
  onSubmit = (event) => {
    const { email, passwordOne } = this.state;
    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then((authUser) => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.SIGN_IN);
      })
      .catch((error) => {
        this.setState({ error });
      });
    event.preventDefault();
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { username, email, passwordOne, passwordTwo, error } = this.state;
    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === "" ||
      email === "" ||
      username === "";
    return (
      <div className="column">
        <Grid
          textAlign="center"
          style={{ height: "100vh" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="teal" textAlign="center">
              <Image src="https://react.semantic-ui.com/logo.png" /> Sign Up
            </Header>
            <Form size="large" onSubmit={this.onSubmit}>
              <Segment stacked>
                <Form.Input
                  fluid
                  name="username"
                  value={username}
                  onChange={this.onChange}
                  icon="user"
                  iconPosition="left"
                  placeholder="Full Name"
                />
                <Form.Input
                  fluid
                  name="email"
                  value={email}
                  onChange={this.onChange}
                  icon="envelope"
                  iconPosition="left"
                  placeholder="Email"
                />
                <Form.Input
                  fluid
                  name="passwordOne"
                  value={passwordOne}
                  onChange={this.onChange}
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                />
                <Form.Input
                  fluid
                  name="passwordTwo"
                  value={passwordTwo}
                  onChange={this.onChange}
                  icon="lock"
                  iconPosition="left"
                  placeholder=" Conform Password"
                  type="password"
                />

                <Button color="teal" fluid size="large" disabled={isInvalid}>
                  SignUp
                </Button>
              </Segment>
              {error && <p>{error.message}</p>}
            </Form>
            <Message>
              Already have Account <Link to={ROUTES.SIGN_IN}>Sign In</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const SignUpLink = () => (
  <Message>
    Don't have account ? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </Message>
);

const SignUpForm = compose(withRouter, withFirebase)(SignUpFormBase);
//withRouter(withFirebse(SignUpFormBase));

export default SignUpPage;
export { SignUpForm, SignUpLink };
