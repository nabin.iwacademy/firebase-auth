import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { SignUpLink } from "../SignUp";
import { Button, Form, Grid, Header, Image, Segment } from "semantic-ui-react";
import * as ROUTES from "../../constants/Routes";
import { withFirebase } from "../Firebase";

const SignInPage = () => {
  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          <Image src="https://react.semantic-ui.com/logo.png" />
          Log-in to your account
        </Header>
        <SignInForm></SignInForm>
        <SignUpLink></SignUpLink>
      </Grid.Column>
    </Grid>
  );
};

const INITIAL_STATE = {
  email: "",
  password: "",
  error: null,
};

class SignInFormBase extends Component {
  state = { ...INITIAL_STATE };
  onSubmit = (event) => {
    const { email, password } = this.state;
    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then((authUser) => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch((error) => {
        this.setState({ error });
      });
    event.preventDefault();
  };
  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    const { error, email, password } = this.state;
    const isInvalid = password === "" || email === "";
    return (
      <Form size="large" onSubmit={this.onSubmit}>
        <Segment stacked>
          <Form.Input
            fluid
            name="email"
            value={email}
            onChange={this.onChange}
            icon="user"
            iconPosition="left"
            placeholder="Email"
          />
          <Form.Input
            fluid
            name="password"
            value={password}
            onChange={this.onChange}
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
          />

          <Button
            color="teal"
            fluid
            size="large"
            disabled={isInvalid}
            type="submit"
          >
            Login
          </Button>
          {error && <p>{error.message}</p>}
        </Segment>
      </Form>
    );
  }
}
const SignInForm = compose(withRouter, withFirebase)(SignInFormBase);

export default SignInPage;
export { SignInForm };
