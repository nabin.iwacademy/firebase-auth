import React from "react";
import { Switch, Route } from "react-router-dom";
import * as routes from "./constants/Routes";
import Landing from "./components/Landing";
import SignUpPage from "./components/SignUp";
import Home from "./components/Home";
import SignInPage from "./components/SignIn";

const NotFound = () => <h1>404- Not Found</h1>;

const Router = () => (
  <Switch>
    <Route exact path={routes.LANDING} component={Landing}></Route>
    <Route path={routes.SIGN_UP} component={SignUpPage}></Route>
    <Route path={routes.HOME} component={Home}></Route>
    <Route path={routes.SIGN_IN} component={SignInPage}></Route>
    <Route component={NotFound}></Route>
  </Switch>
);

export default Router;
